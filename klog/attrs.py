
class OverLogAttr:
    admin_list_display = ['reference', 'disp_log_level', 'log_type', 'action', 'message',
                          'group_ref', 'version', 'date_modified', 'disp_workspace', 'ip_address']
    admin_list_filter = ('is_active', 'log_type', 'action', 'log_level', 'date_modified',)
    admin_fieldsets = [
        ('Basic',
            {'fields': ['log_type', 'action', 'reference']}),
        ('Contact',
            {'fields': ['message', ]}),
    ]
    admin_search_fields = ('reference', 'action', 'ext_content_str', 'ext_content_pk',)
