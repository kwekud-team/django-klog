from enum import Enum


class KLogTypeK(Enum):
    # Frmt: LOG_TYPE_ACTION = 'log_type:action'
    # Eg: ACCOUNTS_REGISTER = 'accounts:register'

    def get_log_type(self):
        return self.value.split(':')[0]

    def get_action(self):
        return self.value.split(':')[1]
