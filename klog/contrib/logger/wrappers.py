import uuid
import logging
from klog.contrib.logger.constants import KLogTypeK
from klog.contrib.logger.dclasses import LogItem

klogger = logging.getLogger('klogger')


class KLoggerWrapper:

    @classmethod
    def _inflate_log_item(cls, workspace, type_action: KLogTypeK, **kwargs):
        return LogItem(
            workspace=workspace,
            log_type=type_action.get_log_type(),
            action=type_action.get_action(),
            ext_content=kwargs.get('ext_content', None),
            reference=kwargs.get('reference', str(uuid.uuid4())),
            group_ref=kwargs.get('group_ref', None),
            message=kwargs.get('message', None),
            user=kwargs.get('user', None),
            request=kwargs.get('request', None)
        )

    @classmethod
    def success(cls, workspace, type_action, **kwargs):
        log_item = cls._inflate_log_item(workspace, type_action, **kwargs)
        log_item.message = log_item.message or 'Request completed successfully'

        klogger.success(log_item.message, exc_info=True, extra=log_item.gen_log_extra())

    @classmethod
    def debug(cls, workspace, type_action, **kwargs):
        log_item = cls._inflate_log_item(workspace, type_action, **kwargs)
        log_item.message = log_item.message or 'Debug'
        
        klogger.debug(log_item.message, exc_info=True, extra=log_item.gen_log_extra())

    @classmethod
    def info(cls, workspace, type_action, **kwargs):
        log_item = cls._inflate_log_item(workspace, type_action, **kwargs)
        log_item.message = log_item.message or 'Info'

        klogger.info(log_item.message, exc_info=True, extra=log_item.gen_log_extra())

    @classmethod
    def error(cls, workspace, type_action, **kwargs):
        log_item = cls._inflate_log_item(workspace, type_action, **kwargs)
        log_item.message = log_item.message or 'An error occurred while handling your request'

        exception = kwargs.get('exception', None)
        if exception:
            log_item.message = f'{log_item.message}. EXCEPTION: \n{exception}.'
            cls._save_remote(log_item, exception)

        klogger.error(log_item.message, exc_info=True, extra=log_item.gen_log_extra())

    @classmethod
    def _save_remote(cls, log_item, exception):
        pass
        # # TODO: generalise so we can use any provider
        # from sentry_sdk import capture_exception
        #
        # log_item.remote_provider = OversightK.LP_SENTRY.value
        # log_item.remote_ref = capture_exception(exception)
