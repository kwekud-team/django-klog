import inspect
from typing import Type
from dataclasses import dataclass, fields
from django.db import models
from django.core.handlers.wsgi import WSGIRequest

from kommons.utils.model import get_str_from_model
from kommons.utils.http import get_client_ip


@dataclass
class LogItem:
    workspace: Type[models.Model]
    action: str
    log_type: str
    ext_content: Type[models.Model]
    reference: str
    group_ref: str
    message: str = ''
    user: Type[models.Model] = None
    remote_provider: str = ''
    remote_ref: str = ''
    ip_address: str = ''

    request: WSGIRequest = None

    PREFIX = 'ov_'

    def gen_log_extra(self) -> dict:
        ignore_list = []

        return {
            f'{self.PREFIX}{field.name}': getattr(self, field.name)
            for field in fields(self)
            if field.name not in ignore_list
        }

    def get_user(self):
        if not self.user and self.request:
            return self.request.user if self.request.user.is_authenticated else None
        return self.user

    def get_ip_address(self):
        if not self.ip_address and self.request:
            return get_client_ip(self.request)
        return self.ip_address

    @staticmethod
    def _get_content_str(content_obj):
        if content_obj:
            return get_str_from_model(content_obj)

    @staticmethod
    def _get_content_pk(content_obj):
        if content_obj:
            return None if inspect.isclass(content_obj) else content_obj.pk

    def get_workspace_str(self):
        return self._get_content_str(self.workspace)

    def get_workspace_pk(self):
        return self._get_content_pk(self.workspace)

    def get_ext_content_str(self):
        return self._get_content_str(self.ext_content)

    def get_ext_content_pk(self):
        return self._get_content_pk(self.ext_content)
