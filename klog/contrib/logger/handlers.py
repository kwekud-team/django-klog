import logging
from klog.contrib.logger.dclasses import LogItem


class KLoggerDbHandler(logging.Handler):

    def emit(self, record):
        self._save_to_db(record)

    def _inflate_log_item(self, record):
        dt = {}
        for x in dir(record):
            if x.startswith(LogItem.PREFIX):
                key = x.replace(LogItem.PREFIX, '')
                dt[key] = getattr(record, x)

        return LogItem(**dt)

    def _get_next_version(self, record, log_item):
        from klog.models import KLog

        uniques = {
            'workspace_str': log_item.get_workspace_str(),
            'workspace_pk': log_item.get_workspace_pk(),
            'log_level': record.levelno,
            'log_type': log_item.log_type,
            'action': log_item.action
        }
        if log_item.ext_content:
            uniques.update({
                'ext_content_str': log_item.get_ext_content_str(),
                'ext_content_pk': log_item.get_ext_content_pk()
            })

        version_qs = KLog.objects.filter(**uniques)
        version_obj = version_qs.order_by('-version').first()
        return version_obj.version + 1 if version_obj else 1

    def _save_to_db(self, record):
        from klog.models import KLog

        log_item = self._inflate_log_item(record)

        user = log_item.get_user() or KLog.get_system_user()

        KLog.objects.get_or_create(
            workspace_str=log_item.get_workspace_str(),
            workspace_pk=log_item.get_workspace_pk(),
            log_type=log_item.log_type,
            action=log_item.action,
            reference=log_item.reference,
            defaults={
                'log_level': record.levelno,
                'message': record.getMessage(),
                'ext_content_str': log_item.get_ext_content_str(),
                'ext_content_pk': log_item.get_ext_content_pk(),
                'group_ref': log_item.group_ref,
                'created_by': user,
                'modified_by': user,
                'version': self._get_next_version(record, log_item),
                'remote_provider': log_item.remote_provider,
                'remote_ref': log_item.remote_ref,
                'ip_address': log_item.get_ip_address()
            })
