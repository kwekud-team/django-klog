import uuid
from django.db import models
from django.conf import settings

from klog.utils import generate_system_user, colorize_log_level


class BaseModel(models.Model):
    guid = models.UUIDField(blank=True)
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)
    date_removed = models.DateTimeField(null=True, blank=True)
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                   related_name='%(app_label)s%(class)s_created_by')
    modified_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE,
                                    related_name='%(app_label)s%(class)s_modified_by')
    is_active = models.BooleanField(default=True)

    class Meta:
        abstract = True

    def save(self, *args, **kwargs):
        self.guid = self.guid or uuid.uuid4()
        super().save(*args, **kwargs)

    @staticmethod
    def get_system_user():
        return generate_system_user('system@example.com')


class KLog(BaseModel):
    """
    Workspace holds info about where the data is being stored. You can assign a site object from django contrib or use
    something like tenants from the django-tenants app
    """
    workspace_str = models.CharField(max_length=100)
    workspace_pk = models.CharField(max_length=50)
    log_level = models.PositiveSmallIntegerField(default=0, db_index=True)
    log_type = models.CharField(max_length=100)  # TODO: rename to log_type
    action = models.CharField(max_length=50)
    message = models.TextField()
    ext_content_str = models.CharField(max_length=100, null=True, blank=True)
    ext_content_pk = models.CharField(max_length=50, null=True, blank=True)

    reference = models.CharField(max_length=100)
    group_ref = models.CharField(max_length=100, null=True, blank=True)
    version = models.PositiveIntegerField(default=0)
    ip_address = models.GenericIPAddressField(null=True, blank=True)
    remote_provider = models.CharField(max_length=100, null=True, blank=True)
    remote_ref = models.CharField(max_length=100, null=True, blank=True)

    def __str__(self):
        return f'{self.log_type}: {self.action}'

    class Meta:
        ordering = ('-date_modified',)
        unique_together = ('workspace_str', 'workspace_pk', 'log_type', 'action', 'reference')

    def disp_log_level(self):
        return colorize_log_level(self.log_level)
    disp_log_level.short_description = 'Log Level'

    def disp_remote(self):
        if self.remote_ref:
            return f'{self.remote_provider}:{self.remote_ref}'
    disp_remote.short_description = 'Remote'

    def disp_workspace(self):
        return f'{self.workspace_str}:{self.workspace_pk}'
    disp_workspace.short_description = 'Workspace'

    def disp_ext_content(self):
        if self.ext_content_pk:
            return f'{self.ext_content_str}:{self.ext_content_pk}'
    disp_ext_content.short_description = 'Ext Content'
