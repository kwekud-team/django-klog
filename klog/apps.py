import logging
from django.apps import AppConfig
from klog.contrib.logger.utils import add_logging_level


class CustomConfig(AppConfig):
    name = 'klog'

    def ready(self):
        add_logging_level('SUCCESS', logging.INFO + 1)
