import logging
from django.contrib.auth import get_user_model
from django.utils.html import format_html


def generate_system_user(username=None):
    username = username or 'system@example.com'
    user_model = get_user_model()
    user = user_model.objects.filter(username=username).first()
    return user or user_model.objects.create_user(username)


def colorize_log_level(log_level):
    if log_level in [logging.SUCCESS]:
        color = 'green'
    elif log_level in [logging.NOTSET, logging.INFO]:
        color = 'blue'
    elif log_level in [logging.WARNING, logging.DEBUG]:
        color = 'orange'
    else:
        color = 'red'
    return format_html('<span style="color: {color};">{msg}</span>', color=color,
                       msg=logging.getLevelName(log_level))