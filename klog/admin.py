from django.contrib import admin

from klog.models import KLog
from klog.attrs import OverLogAttr


@admin.register(KLog)
class OverLogAdmin(admin.ModelAdmin):
    list_display = OverLogAttr.admin_list_display
    list_filter = OverLogAttr.admin_list_filter
    fieldsets = OverLogAttr.admin_fieldsets
    search_fields = OverLogAttr.admin_search_fields
